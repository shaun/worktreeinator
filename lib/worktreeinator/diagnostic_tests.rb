module Worktreeinator
  class DiagnosticTests
    def tmuxinator_installed?
      Kernel.system("command -v tmuxinator > /dev/null")
    end

    def valid_tmuxinator_project_exists?
      local_tmuxinator_project_exists? ||
        tmuxinator_project_matches_current_directory_name?
    end

    def local_tmuxinator_project_exists?
      File.exist?(File.join(Dir.pwd, ".tmuxinator.yml"))
    end

    def currently_inside_a_git_repository?
      currently_inside_a_bare_git_repository? ||
        currently_inside_a_regular_git_repository?
    end

    private

    def tmuxinator_project_matches_current_directory_name?
      current_directory = File.basename(Dir.pwd)
      tmuxinator_config_directory = "~/.config/tmuxinator"
      matching_tmuxinator_project = File.join(File.expand_path(tmuxinator_config_directory, current_directory))

      File.exist?(matching_tmuxinator_project)
    end

    def currently_inside_a_bare_git_repository?
      result = Kernel.system("git rev-parse --is-bare-repository > /dev/null 2>&1")

      result == "true"
    end

    def currently_inside_a_regular_git_repository?
      git_directory = File.join(Dir.pwd, ".git")

      File.directory?(git_directory)
    end
  end
end
