module Worktreeinator
  class Doctor
    def initialize(diagnostic_tests: Worktreeinator::DiagnosticTests.new)
      @diagnostic_tests = diagnostic_tests
    end

    def call
      check_if_tmuxinator_is_installed
      check_for_valid_tmuxinator_project
      check_if_currently_inside_git_repository
    end

    private

    attr_reader :diagnostic_tests

    TMUXINATOR_NOT_INSTALLED_HELP_TEXT = <<~HELP_TEXT
      Tmuxinator needs to be installed in order to use
      worktreeinator.

      Tmuxinator can be installed with the command:
      gem install tmuxinator

      More information about tmuxinator can be found here:
      https://github.com/tmuxinator/tmuxinator
    HELP_TEXT

    MISSING_TMUXINATOR_PROJECT_HELP_TEXT = <<~HELP_TEXT
      I couldn't find a valid tmuxinator project.

      A tmuxinator project can be defined in the current working
      directory, in this case the file should be named:
      .tmuxinator.yml

      A tmuxinator project can also be defined in the directory
      ~/.tmuxintator, in this case the file should be named:
      #{File.basename(Dir.pwd)}.yml

      More information about tmuxinator projects can be found here:
      https://github.com/tmuxinator/tmuxinator#usage
    HELP_TEXT

    INVALID_DIRECTORY_HELP_TEXT = <<~HELP_TEXT
      Worktreeinator commands need to be run from within a git
      project.

      The current directory doesn't seem to be a git project.
    HELP_TEXT

    def check_if_tmuxinator_is_installed
      Thor.new.say "----------------------------------------------------------------"
      Thor.new.say "Checking if tmuxinator is installed ==> "

      friendly_diagnostics_result(
        diagnostic_tests.tmuxinator_installed?,
        help_text: TMUXINATOR_NOT_INSTALLED_HELP_TEXT
      )
    end

    def check_for_valid_tmuxinator_project
      Thor.new.say "----------------------------------------------------------------"
      Thor.new.say "Checking for a valid tmuxinator project ==> "

      friendly_diagnostics_result(
        diagnostic_tests.valid_tmuxinator_project_exists?,
        help_text: MISSING_TMUXINATOR_PROJECT_HELP_TEXT
      )
    end

    def check_if_currently_inside_git_repository
      Thor.new.say "----------------------------------------------------------------"
      Thor.new.say "Checking if we're currently inside a git repository ==> "

      friendly_diagnostics_result(
        diagnostic_tests.currently_inside_a_git_repository?,
        help_text: INVALID_DIRECTORY_HELP_TEXT
      )

      Thor.new.say "----------------------------------------------------------------"
    end

    def friendly_diagnostics_result(result, help_text:)
      if result == true
        Thor.new.say("Yes", :green)
      else
        Thor.new.say("No", :red)
        Thor.new.say("")
        Thor.new.say(help_text)
      end
    end
  end
end
