# frozen_string_literal: true

require "thor"

module Worktreeinator
end

require_relative "worktreeinator/version"
require_relative "worktreeinator/diagnostic_tests"
require_relative "worktreeinator/doctor"
require_relative "worktreeinator/cli"
