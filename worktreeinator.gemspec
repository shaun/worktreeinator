# frozen_string_literal: true

require_relative "lib/worktreeinator/version"

Gem::Specification.new do |spec|
  spec.name = "Worktreeinator"
  spec.version = Worktreeinator::VERSION
  spec.authors = ["Shaun"]
  spec.email = ["76047-shaun@users.noreply.framagit.org"]

  spec.summary = "A tmuxinator companion for working with Git worktrees"
  spec.description = "Worktreeinator allows you to start a new tmuxinator project for each Git worktree."
  spec.homepage = "https://framagit.org/shaun/worktreeinator"
  spec.license = "Nonstandard"
  spec.required_ruby_version = ">= 2.6.0"

  spec.metadata["allowed_push_host"] = "TODO: Set to your gem server 'https://example.com'"
  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://framagit.org/shaun/worktreeinator"
  spec.metadata["changelog_uri"] = "https://framagit.org/shaun/worktreeinator/-/blob/main/CHANGELOG.md"

  spec.files = Dir["lib/**/*", "spec/**/*", "bin/*"]
  spec.require_paths = ["lib"]
  spec.bindir = "bin"
  spec.executables << "worktreeinator"

  spec.add_dependency "thor", "~> 1.2.0"
end
