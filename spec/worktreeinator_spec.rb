# frozen_string_literal: true

RSpec.describe Worktreeinator do
  it "has a version number" do
    expect(Worktreeinator::VERSION).not_to be nil
  end
end
