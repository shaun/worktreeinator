require "spec_helper"

RSpec.describe Worktreeinator::DiagnosticTests do
  let(:diagnostic_tests) { Worktreeinator::DiagnosticTests.new }

  describe "#tmuxinator_installed?" do
    context "when tmuxinator is installed" do
      before do
        allow(Kernel).to receive(:system)
          .with("command -v tmuxinator > /dev/null")
          .and_return(true)
      end

      it "returns true" do
        expect(diagnostic_tests.tmuxinator_installed?).to eq(true)
      end
    end

    context "when tmuxinator is not installed" do
      before do
        allow(Kernel).to receive(:system)
          .with("command -v tmuxinator > /dev/null")
          .and_return(false)
      end

      it "returns false" do
        expect(diagnostic_tests.tmuxinator_installed?).to eq(false)
      end
    end
  end

  describe "#valid_tmuxinator_project_exists?" do
    let(:current_working_directory) { "/my-directory" }
    let(:default_tmuxinator_config_directory) { "~/.config/tmuxinator" }
    let(:named_tmuxinator_project) { "~/.config/tmuxinator/my-tmuxinator-project" }

    before do
      allow(Dir).to receive(:pwd)
        .and_return(current_working_directory)

      allow(File).to receive(:exist?)
        .with(anything)
        .and_return(false)

      allow(File).to receive(:expand_path)
        .with(
          default_tmuxinator_config_directory,
          File.basename(current_working_directory)
        )
        .and_return(named_tmuxinator_project)
    end

    it "checks for `.tmuxinator.yml` in the current working directory" do
      expect(File).to receive(:exist?)
        .with("#{current_working_directory}/.tmuxinator.yml")
        .and_return(true)

      diagnostic_tests.valid_tmuxinator_project_exists?
    end

    context "when a local `.tmuxinator.yml` project exists" do
      before do
        allow(File).to receive(:exist?)
          .with("#{current_working_directory}/.tmuxinator.yml")
          .and_return(true)
      end

      it "returns true" do
        expect(diagnostic_tests.valid_tmuxinator_project_exists?).to eq(true)
      end
    end

    context "when `.tmuxinator.yml` doesn't exist in the current working directory" do
      before do
        allow(File).to receive(:exist?)
          .with("#{current_working_directory}/.tmuxinator.yml")
          .and_return(false)
      end

      it "checks for a correctly named tmuxinator project (in tmuxinator's default config directory)" do
        expect(File).to receive(:expand_path)
          .with(
            default_tmuxinator_config_directory,
            File.basename(current_working_directory)
          )
          .and_return(named_tmuxinator_project)

        expect(File).to receive(:exist?)
          .with(named_tmuxinator_project)
          .and_return(true)

        diagnostic_tests.valid_tmuxinator_project_exists?
      end

      context "when a correctly named tmuxinator project exists" do
        before do
          allow(File).to receive(:exist?)
            .with(named_tmuxinator_project)
            .and_return(true)
        end

        it "returns true" do
          expect(diagnostic_tests.valid_tmuxinator_project_exists?).to eq(true)
        end
      end
    end

    context "when a valid tmuxinator project can't be found" do
      before do
        allow(File).to receive(:exist?)
          .with("#{current_working_directory}/.tmuxinator.yml")
          .and_return(false)

        allow(File).to receive(:exist?)
          .with(named_tmuxinator_project)
          .and_return(false)
      end

      it "returns false" do
        expect(diagnostic_tests.valid_tmuxinator_project_exists?).to eq(false)
      end
    end
  end

  describe "#local_tmuxinator_project_exists?" do
    let(:current_working_directory) { "/my-cool-project" }

    before do
      allow(Dir).to receive(:pwd)
        .and_return(current_working_directory)
    end

    context "when a local `.tmuxinator.yml` project exists" do
      before do
        allow(File).to receive(:exist?)
          .with("#{current_working_directory}/.tmuxinator.yml")
          .and_return(true)
      end

      it "returns true" do
        expect(diagnostic_tests.local_tmuxinator_project_exists?).to eq(true)
      end
    end

    context "when a local `.tmuxinator.yml` project doesn't exist" do
      before do
        allow(File).to receive(:exist?)
          .with("#{current_working_directory}/.tmuxinator.yml")
          .and_return(false)
      end

      it "returns false" do
        expect(diagnostic_tests.local_tmuxinator_project_exists?).to eq(false)
      end
    end
  end

  describe "#currently_inside_a_git_repository?" do
    let(:current_working_directory) { "/my-project" }

    before do
      allow(Kernel).to receive(:system)
        .with(anything)

      allow(Dir).to receive(:pwd)
        .and_return(current_working_directory)

      allow(File).to receive(:directory?)
        .with(anything)
    end

    context "when currently inside a bare git repository" do
      before do
        allow(Kernel).to receive(:system)
          .with("git rev-parse --is-bare-repository > /dev/null 2>&1")
          .and_return("true")
      end

      it "returns true" do
        expect(diagnostic_tests.currently_inside_a_git_repository?).to eq(true)
      end
    end

    context "when currently inside a regular git repository (and not inside a bare repository)" do
      before do
        allow(File).to receive(:directory?)
          .with(File.join(current_working_directory, ".git"))
          .and_return(true)
      end

      it "returns true" do
        expect(diagnostic_tests.currently_inside_a_git_repository?).to eq(true)
      end
    end

    context "when not currently inside a bare repository or a regular git repository" do
      before do
        allow(Kernel).to receive(:system)
          .with("git rev-parse --is-bare-repository > /dev/null 2>&1")
          .and_return("false")

        allow(File).to receive(:directory?)
          .with(File.join(current_working_directory, ".git"))
          .and_return(false)
      end

      it "returns false" do
        expect(diagnostic_tests.currently_inside_a_git_repository?).to eq(false)
      end
    end
  end
end
