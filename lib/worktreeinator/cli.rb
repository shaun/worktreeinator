module Worktreeinator
  class Cli < Thor
    desc "doctor", "The doctor will diagnose issues related to worktreeinator"

    def doctor
      Worktreeinator::Doctor.new.call
    end
  end
end
